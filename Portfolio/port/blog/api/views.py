from .serializers import HomeSerializer, PostSerializer, PostDetailSerializer, CommentCreateSerializer, ProjectSerializer, FileSerializer, ProjectCreateSerializer
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import filters,  viewsets
from rest_framework.response import Response
from rest_framework import status
from blog.models import Post, Comment, Project
from django_filters.rest_framework import DjangoFilterBackend
from .service import PaginationPost
from django.db.models import Q
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.decorators import action

class HomeAPIView(ListAPIView):
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)
    pagination_class = PaginationPost
    queryset = Post.objects.all()
    serializer_class = HomeSerializer
    model = Post

class PostAPIView(ListAPIView):
    serializer_class = PostSerializer
    model = Post
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['slug']
    lookup_field = 'slug'
    def get_queryset(self):
        return Post.objects.filter(category__slug=self.kwargs.get("slug")).select_related('category')##Фильтруем посты по категориям

class PostDetaiAPIlView(ListAPIView):
    serializer_class = PostDetailSerializer
    def get_queryset(self):
        return Post.objects.filter(
            Q(category__slug=self.kwargs.get("slug")) &
            Q(slug=self.kwargs.get("post_slug"))
        ).select_related('category')
    model = Post


class CreateAPIComment(ListCreateAPIView):
    def get_queryset(self):
        return Comment.objects.filter(post__id=self.kwargs.get("pk")).select_related('post')
    model = Comment
    serializer_class = CommentCreateSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )

    def perform_create(self, request):
        serializer = CommentCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProjectViewSet(viewsets.ModelViewSet):
    # serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    queryset = Project.objects.all()

    def get_serializer_class(self):
        if self.action == 'file_create':
            return FileSerializer
        if self.action in ['create']:
            return ProjectCreateSerializer
        return ProjectSerializer

    @action(detail=True, methods=['post'])
    def file_create(self, request, pk=None):
        project = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            project.file_project = serializer.validated_data['file_project']
            project.save()
            return Response({'status': 'file_project успешно изменен'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False)
    def projects(self, request):
        projects = Project.objects.all()
        return Response({'post': [post.name for post in projects]})


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostDetailSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
