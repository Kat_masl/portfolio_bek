from django.urls import path, include
from .views import PostAPIView, HomeAPIView, PostDetaiAPIlView, CreateAPIComment, ProjectViewSet, PostViewSet
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'create', ProjectViewSet)
router.register(r'create-post', ProjectViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('post/', HomeAPIView.as_view(), name="post_list"),
    path('post/comment/<int:pk>/', CreateAPIComment.as_view(), name="post_comment"),
    path('post/<slug:slug>/', PostAPIView.as_view(), name="post_api"),
    path('post/<slug:slug>/<slug:post_slug>/', PostDetaiAPIlView.as_view(), name="postdet_api"),
]
