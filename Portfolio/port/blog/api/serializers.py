from multiprocessing import Value
from rest_framework import serializers
from blog.models import Project, Post, Comment

class HomeSerializer(serializers.ModelSerializer):
    author=serializers.SlugRelatedField(slug_field="username", read_only=True )
    category=serializers.SlugRelatedField(slug_field="slug", read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'category', 'title', 'author', 'text')

class PostSerializer(serializers.ModelSerializer):
    author=serializers.SlugRelatedField(slug_field="username", read_only=True )
    category=serializers.SlugRelatedField(slug_field="name", read_only=True )

    class Meta:
        model = Post
        fields = ('id', 'category', 'title', 'author', 'text', 'slug')

class CommentCreateSerializer(serializers.ModelSerializer):
    # post=serializers.SlugRelatedField(slug_field="title", read_only=True )
    website = serializers.CharField()
    message = serializers.CharField()
    class Meta:
        model = Comment
        fields = ['name', 'email', 'website', 'message', 'post']

    def validate(self, data):
        if data['website'] == data['message']:
            raise serializers.ValidationError('Сайт и сообщение не могут быть одинаковыми')
        return data

    def validate_message(self, value):
        if len(value) <10:
            raise serializers.ValidationError(f'Hазвание должно быть минимум 10 символов, вы ввели {len(value)}')
        return value

class ProjectCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['name', 'description', 'creation_theoru', 'file_project', 'file_document', 'post']

class ProjectSerializer(serializers.ModelSerializer):
    post=serializers.SlugRelatedField(slug_field="title", read_only=True )
    class Meta:
        model = Project
        fields = ['name', 'description', 'creation_theoru', 'file_project', 'file_document', 'post']

class PostDetailSerializer(serializers.ModelSerializer):
    author=serializers.SlugRelatedField(slug_field="username", read_only=True )
    category=serializers.SlugRelatedField(slug_field="name", read_only=True )
    tags=serializers.SlugRelatedField(slug_field="name", read_only=True, many=True )
    event=serializers.SlugRelatedField(slug_field="name", read_only=True, many=True )
    projects = ProjectSerializer(many=True, read_only=True)
    comment = CommentCreateSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ['author', 'title', 'text', 'image', 'category', 'tags', 'event', 'slug', 'projects', 'comment']

class FileSerializer(serializers.Serializer):
    file_project = serializers.URLField()