from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

class PaginationPost(PageNumberPagination):
    page_size = 3
    max_page_size = 100
    def get_paginated_response(self, data): ##Данный метод говорит о том, каким образом мы будем выводить информацию о нашей пагинации
        return Response({
            'links': {#Здесь указываем два ключа 3 ключа это links следующая ссылка и предыдущая ссылка
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data
    })