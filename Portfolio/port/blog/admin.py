from django.contrib import admin
from django import forms
from . import models
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin

from ckeditor_uploader.widgets import CKEditorUploadingWidget

from blog.models import Project

class ProjectAdminForm(forms.ModelForm):
    description_ru = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())
    description_en = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())
    creation_theoru_ru = forms.CharField(label="Теория", widget=CKEditorUploadingWidget())
    creation_theoru_en = forms.CharField(label="Теория", widget=CKEditorUploadingWidget())

    class Meta:
        model = Project
        fields = '__all__'

class ProjectInline(admin.StackedInline):
    model = models.Project
    extra = 1

@admin.register(models.Post)
class PostAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ["id", "author", "title", "create_at", "category"]
    inlines = [ProjectInline]
    save_as = True #Сохранить как новый объект, чтобы нам не вписывать каждый раз новый пост, а просто его копировать
    save_on_top = True#Кнопка сохранить в админке будет и наверху, чтобы нам не листать вниз
    list_filter = ["category"]
    search_fields = ["category__name__startswith"]

@admin.register(models.Project)
class ProjectAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ["name", "post"]
    form = ProjectAdminForm

@admin.register(models.Comment)
class CommentAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ['name', 'email', 'website', 'create_at', 'id']

@admin.register(models.Category)
class CategoryAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ("name", "slug")

@admin.register(models.Tag)
class TagAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ("name", "slug")

@admin.register(models.Event)
class EventAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = ("name", "slug")
