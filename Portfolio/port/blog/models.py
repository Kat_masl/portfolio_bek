from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.urls import reverse


class Category(models.Model):
    name = models.CharField('Название', max_length=100)
    slug = models.SlugField('УФ URL', max_length=100)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Tag(models.Model):
    name = models.CharField('Название', max_length=100)
    slug = models.SlugField('УФ URL', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'

class Event(models.Model):
    name = models.CharField('Название', max_length=100)
    slug = models.SlugField('УФ URL', max_length=100)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Мероприятия'
        verbose_name_plural = 'Мероприятия'


class Post(models.Model):
    author = models.ForeignKey( User, related_name="posts", verbose_name='Автор', on_delete=models.CASCADE)##Несколько авторов к одной категории
    title = models.CharField('Название', max_length=200)
    text = models.TextField('Текст')
    image = models.ImageField('Картинка', upload_to='articles/', null=True, blank=True)
    create_at = models.DateTimeField('Дата статьи', auto_now_add=True)
    category = models.ForeignKey(
        Category,
        related_name="post",
        verbose_name='Категории',
        on_delete=models.SET_NULL,##Удаляем категорию, а пост остается
        null=True
    )
    tags = models.ManyToManyField(Tag, related_name="post", verbose_name='Tэги')
    event = models.ManyToManyField(Event, related_name="post", verbose_name='Мероприятия')
    slug = models.SlugField(max_length=200, default='', unique=True) #Уникальное поле


    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("post_single", kwargs={"slug": self.category.slug, "post_slug": self.slug})

    def get_projects(self):
        return self.projects.all()

    def get_comments(self):
        return self.comment.all()


class Project(models.Model):
    name = models.CharField('Название', max_length=100)
    description = models.TextField('Описание')
    creation_theoru = models.TextField('Теория')
    file_project = models.URLField('Файл_проекта')
    file_document = models.URLField('Документ')
    post = models.ForeignKey(
        Post,
        related_name="projects",
        verbose_name='Посты',
        on_delete=models.SET_NULL,##Проект удаляем, а пост оставляем
        null=True,
        blank=True
    )


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

class Comment(models.Model):
    name = models.CharField('Имя', max_length=50)
    email = models.CharField('Эмэил', max_length=100)
    website = models.CharField('Сайт', max_length=150, blank=True, null=True)
    message = models.TextField('Сообщение', max_length=500)
    create_at = models.DateTimeField('Дата коммента',default=timezone.now)
    post = models.ForeignKey(Post, related_name="comment", verbose_name='Посты', on_delete=models.CASCADE, null=True)##Удаляе кпост удаляются и комментарии



    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

