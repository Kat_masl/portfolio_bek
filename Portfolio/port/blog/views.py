from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView
from .models import Post, Comment
from .forms import CommentForm, CustomerRegistrationForm, PostForm, ProjcetForm
from django.views import View
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .permissions import IsAuthorOrIsAdminOrReadOnly

class HomeView(ListView):
    model = Post
    template_name = "blog/home.html"

class PostView(ListView):
    model = Post
    def get_queryset(self):
        return Post.objects.filter(category__slug=self.kwargs.get("slug")).select_related('category')##Фильтруем посты по категориям

@method_decorator(login_required, name='get')
class PostDetailView(IsAuthorOrIsAdminOrReadOnly, DetailView):
    model = Post
    context_object_name = "post"
    slug_url_kwarg = 'post_slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm()
        return context

class CreateComment(CreateView):
    model = Comment
    form_class = CommentForm

    def form_valid(self, form):
        form.instance.post_id = self.kwargs.get('pk')
        self.object = form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.post.get_absolute_url()

class Search(ListView):
    #Поиск по названию

    def get_queryset(self):
        return Post.objects.filter(title__icontains =self.request.GET.get("q"))

    def get_context_data(self, *args ,**kwargs):
        context = super().get_context_data(*args ,**kwargs)
        context["q"] = self.request.GET.get("q")
        return context

class CustomerRegistrationView(View):
    def get(self, request):
        form = CustomerRegistrationForm()
        return render(request, 'blog/customerregistration.html', {'form': form})

    def post(self, request):
        form = CustomerRegistrationForm(request.POST)
        if form.is_valid():
            messages.success(request, 'Поздравляю!! Вы успешно зарегистрировались')
            form.save()
        return render(request, 'blog/customerregistration.html', {'form': form})

    # def checkout(request):

def login(request):
 return render(request, 'blog/login.html')

def profile(request):
    return render(request, 'blog/profile.html')

def create(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
    form = PostForm()

    return render(request,'blog/create_post.html',{'form': form})

def create_project(request):
    if request.method == 'POST':
        form = ProjcetForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    form1 = ProjcetForm()

    return render(request,'blog/create_project.html',{'form1': form1})

def update(request, post_id):
	venue = Post.objects.get(pk=post_id)
	form = PostForm(request.POST or None, request.FILES or None, instance=venue)
	if form.is_valid():
		form.save()
		return redirect('/')

	return render(request, 'blog/update_post.html', 
		{'venue': venue,
		'form':form})

def delete(request, post_id):
	venue = Post.objects.get(pk=post_id)
	venue.delete()
	return redirect('/')	
        





    
