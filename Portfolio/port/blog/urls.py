from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth import views as auth_views
from .forms import LoginForm
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('delete/<post_id>', views.delete, name='delete'),
    path('update/<post_id>', views.update, name='update'),
    path('create-project/', views.create_project, name='create_post'),
    path('create/', views.create, name='create'),
    path('logout/', auth_views.LogoutView.as_view(next_page='login'), name='logout'),
    path('profile/', views.profile, name='profile'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='blog/login.html', authentication_form=LoginForm), name='login'),
    path('registration/', views.CustomerRegistrationView.as_view(), name='customerregistration'),
    path('search', views.Search.as_view(), name="search"),
    path('comment/<int:pk>/', views.CreateComment.as_view(), name="create_comment"),
    path('<slug:slug>/<slug:post_slug>/', views.PostDetailView.as_view(), name="post_single"),
    path('<slug:slug>/', views.PostView.as_view(), name="post_list"),
    path('', views.HomeView.as_view(), name="home"),
]+ static(settings.MEDIA_URL, document_root =  settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)