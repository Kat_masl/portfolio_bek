from modeltranslation.translator import register, TranslationOptions
from blog.models import Category, Tag, Event, Post, Project, Comment

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Tag)
class TagTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Event)
class EventTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'text')

@register(Project)
class ProjectTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'creation_theoru')

@register(Comment)
class CommentTranslationOptions(TranslationOptions):
    fields = ('name', 'message')