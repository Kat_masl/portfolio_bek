from import_export import resources
from blog.models import Category, Tag, Event, Post, Project, Comment

class CategoryResources(resources.ModelResource):
    class Meta:
        model = Category

class TagResources(resources.ModelResource):
    class Meta:
        model = Tag

class EventResources(resources.ModelResource):
    class Meta:
        model = Event

class PostResources(resources.ModelResource):
    class Meta:
        model = Post

class ProjectResources(resources.ModelResource):
    class Meta:
        model = Project


class CommentResources(resources.ModelResource):
    class Meta:
        model = Comment